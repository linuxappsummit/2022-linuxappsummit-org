---
layout: page
title: "Sponsors"
permalink: /sponsors/
style: sponsors-page
---

# LAS Sponsors

Would you like to help make the Linux App Summit possible? Join us and [sponsor](https://linuxappsummit.org/sponsor/)!

## TRENDSETTER

### Canonical

<img class="sponsorlogo" src="/assets/canonical.png" alt="Canonical"/>

> Canonical is the publisher of Ubuntu, the OS for most public cloud workloads as well as the emerging categories of smart gateways, self-driving cars and advanced robots. Canonical provides enterprise security, support and services to commercial users of Ubuntu. Established in 2004, Canonical is a privately held company.
>
> [https://canonical.com](https://canonical.com)

### JingLing

<img class="sponsorlogo" src="/assets/jingling.png" alt="JingLing"/>

> JingLing is the publisher of JingOS; JingOS is a ‘convergence’ Linux-Based Open-Source mobile OS for tablets and mobile devices. It can run Linux and Android apps. It is adapted to run naturally with touch, pencil, keyboard, and trackpad. You can switch between laptop and tablet modes at any time.
JingLing is also the creator of JingPad A1, the World’s FIRST Consumer-level Linux Tablet. JingPad allows you to switch between tablet mode and a standard laptop mode. It has a 2K+ Display and 5G/4G support, which can work with a detachable keyboard, trackpad, and stylus.
>
> [https://jingos.com](https://jingos.com)

## CHAMPION

### Collabora

<img class="sponsorlogo" src="/assets/collabora.svg" alt="Collabora"/>

> Collabora is a leading global consultancy specializing in delivering the benefits of Open Source software to the commercial world. For over 15 years, we've helped clients navigate the ever-evolving world of Open Source, enabling them to develop the best solutions – whether writing a line of code or shaping a longer-term strategic software development plan. By harnessing the potential of community-driven Open Source projects, and re-using existing components, we help our clients reduce time to market and focus on creating product differentiation.
>
> [https://collabora.com](https://collabora.com) <a href="https://twitter.com/Collabora" title="@Collabora"><svg class="svg-icon"><use xlink:href="{{ '/assets/minima-social-icons.svg#twitter' | relative_url }}"></use></svg></a>

### Codethink

<img class="sponsorlogo" src="/assets/codethink.svg" alt="Codethink"/>

> Codethink is an ethical, independent and versatile software services company, based in Manchester, UK. We are expert in the use of Open Source technologies for systems software engineering.
>
> Leveraging Open Source tools and techniques, we help customers to meet demanding challenges with unique innovations and accelerated productivity.
>
> We can analyse, develop, integrate, optimise and support bespoke solutions for your systems in IoT, automotive, finance and anything in between.
>
> [https://www.codethink.co.uk](https://www.codethink.co.uk)


## BACKER

### openSUSE

<img class="sponsorlogo" src="/assets/opensuse.png" alt="openSUSE"/>

> The openSUSE project is a worldwide effort that promotes the use of Linux everywhere. The openSUSE community develops and maintains a packaging and distribution infrastructure, which provides the foundation for the world’s most flexible and powerful Linux distribution.
>
> Our community works together in an open, transparent and friendly manner as part of the global Free and Open Source Software community.
>
> [https://opensuse.org](https://opensuse.org)

### MBition

<img class="sponsorlogo" src="/assets/mbition.svg" alt="MBition"/>

> At MBition, we strive to bring digital luxury to mobility users around the world. To accomplish this, we are redefining how software is developed within the automotive field as part of an international Mercedes-Benz software development network. As a 100% subsidiary of Mercedes-Benz AG, we develop and integrate the next generation of Mercedes-Benz in-car infotainment systems that are based on the Mercedes-Benz Operating System (MBOS). But our contribution to MBOS, the future of car software at Mercedes-Benz, does not stop here. To provide the most seamless & connected mobility experiences, we further engage in Advanced Driver Assistance Systems (ADAS) platform development and continuously improve our Mercedes me companion app for our customers.
>
> [https://mbition.io/](https://mbition.io/)

### Fedora

<img class="sponsorlogo" src="/assets/fedora.svg" alt="Fedora"/>

> The Fedora Project is a community of people who create an innovative, free, and open source platform for hardware, clouds, and containers that enables software developers and community members to build tailored solutions for their users.
>
> [https://getfedora.org/](https://getfedora.org/)

## SUPPORTER

### Tuxedo Computers

<img class="sponsorlogo" src="/assets/tuxedo.svg" alt="Tuxedo"/>

> TUXEDO Computers builds tailor-made hardware with Linux!
>
> For a wide variety of computers and notebooks - all of them individually
built und prepared - we are the place to go. From lightweight ultrabooks
up to full-fledged AI development stations TUXEDO covers every aspect of
modern Linux-based computing.
> In addition to that we provide customers with full service at no extra
cost: Self-programmed driver packages, tech support, fully automated
installation services and everything around our hardware offerings.
>
> [https://www.tuxedocomputers.com](https://www.tuxedocomputers.com)

### Pine64

<img class="sponsorlogo" src="/assets/pine.png" alt="Pine64"/>

> PINE64 is a community driven project that creates Arm and RISC-V devices for open source enthusiasts and industry partners. Perhaps best known for the PinePhone, our Linux-only smartphone, and the Pinebook range of laptops, we strive to deliver devices that you want to use and develop for. Rather than applying business to a FOSS setting, we allow FOSS principles to guide our business.
>
> [https://www.pine64.org/](https://www.pine64.org/)

### Shells

<img class="sponsorlogo" src="/assets/shells.png" alt="Shells"/>

> At Shells, we are closing the digital divide by providing simple, secure, and affordable access to a powerful, future-proof computing solution that is accessible on any device.  In addition to providing a powerful computing option, we also offer a variety of pre-installed Linux distributions which give our users complete flexibility and control of their device while also opening new doors for a potential new Linux user.
>
> [https://www.shells.com](https://www.shells.com)

### Slimbook

<img class="sponsorlogo" src="/assets/slimbook.svg" alt="Slimbook"/>

> SLIMBOOK has been in the computer manufacturing business since 2015, we build computers tailored for Linux environments and ship them worldwide.
>
> Delivering quality hardware with our own apps combined with an unrivaled tech support team to improve the end user experience is our main goal.
>
> We also firmly believe that not everything is about the hardware, SLIMBOOK has been involved from the beginning with the community. We started taking on small local projects aiming to bring the GNU/Linux ecosystem to everyone, partnering with state of the art Linux desktops like KDE among other OS's. And of course > having our very own Linux academy, "Linux Center", where free Linux / FOSS courses are imparted regularly for everyone.
>
> If you love Linux and need quality hardware to match, BE ONE OF US.
>
> [https://www.slimbook.es](https://www.slimbook.es)

## GitLab

<img class="sponsorlogo" src="/assets/gitlab.svg" alt="GitLab"/>

> GitLab is a complete DevOps platform, delivered as a single application, fundamentally changing the way Development, Security, and Ops teams collaborate. GitLab helps teams accelerate software delivery from weeks to minutes, reduce development costs, and reduce the risk of application vulnerabilities while increasing developer productivity. GitLab provides unmatched visibility, radical new levels of efficiency and comprehensive governance to significantly compress the time between planning a change and monitoring its effect.
>
> Built on Open Source, GitLab works alongside its community of thousands of contributors and millions of users to continuously deliver new DevOps innovations. More than 100,000 organizations from startups, to leading open source projects, to global enterprise organizations, trust GitLab to deliver great software at new speeds.
>
> [https://about.gitlab.com/solutions/open-source](https://about.gitlab.com/solutions/open-source)

### Red Hat

<img class="sponsorlogo" src="/assets/redhat.png" alt="Red Hat"/>

> Red Hat is an enterprise software company with an open source
> development model. With engineers connected to open source communities,
> the freedom of our subscription model, and a broad portfolio of products
> that's constantly expanding, Red Hat is here to help you face your
> business challenges head-on.
>
> [https://redhat.com](https://redhat.com)

### ITRenew

<img class="sponsorlogo" src="/assets/itrenew.jpg" alt="ITRenew"/>

> To prove that financial success is not incompatible with having a positive impact on our environment and society.
>
> For more than two decades ITRenew has been maximizing the lifetime value of data center technology through innovative circular economic models and a comprehensive portfolio of decommissioning and data security services, edge and component products, and rack-scale compute and storage solutions

## MEDIA PARTNERS

### Linux Magazine

<img class="sponsorlogo" src="/assets/linuxmagazine.png" alt="Linux Magazine"/>

> Linux Magazine is your guide to the world of Linux and open source. Each monthly issue includes advanced technical information you won't find anywhere else including tutorials, in-depth articles on trending topics, troubleshooting and optimization tips, and more! Subscribe to our free newsletters and get news and articles in your inbox.
>
> [https://www.linux-magazine.com/](https://www.linux-magazine.com/)
