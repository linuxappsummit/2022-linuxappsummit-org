---
layout: page
title: "Call for papers"
permalink: /cfp/
---

## LAS 2021 Call for papers

The Linux App Summit (LAS) is designed to accelerate the growth of the Linux application ecosystem by bringing together everyone involved in creating a great Linux application user experience.

Here are some categories that could hold interesting talks. We encourage you to submit a talk by March 22nd, 2021 even if you don’t think it fits into one of these topics.

Regular talks will be 40', lightning talks will be 10', preferrably prerecorded.

### ECOSYSTEM GROWTH

The growth of Linux-based hardware offers more choices in the market and indicates further investment into the Linux app space. App Stores are offering thousands of applications and games for the Linux platform and open source is growing necessity in tech.

How can we ensure continued growth for the Linux app ecosystem? Are there areas of opportunity we should explore? How can we build sustainability into our ecosystem? How do you keep your users engaged?

### PLATFORM DIVERSITY

The Linux platform’s greatest strength is the wealth of diversity in our ecosystem. There are toolkits and software languages for every purpose. Whether you want to develop a game, design an application, or even build a website, the app ecosystem has the tools to make that happen.

Are you working on a technology that enables cross-platform distribution? Do you have ideas for how we can enable platform diversity? How should we evolve to reach your users?

### INNOVATION

Important technologies that you use on a daily basis (maybe without realizing it), have been developed within the Linux app ecosystem. Discuss with us how to push the boundaries of what we are offering right now and reach more users than ever before.

What does the Linux app ecosystem need to get to the top? What are we missing? Are there new technologies we should be embracing?

### PEOPLE & COMMUNICATIONS

While the Linux app ecosystem has been largely shaped by open source communities, we believe there's space for everyone. Making sure both businesses and communities alike can thrive in the ecosystem is vital for the platform to be sustainable over time. It is important for people to be able to make a living from the work they do for Linux users.

How can we make sure companies find their niche in the ecosystem? How can we help communities work together to create and support end-user apps? How can we keep people around in the long-run?

### LEGAL AND LICENSING

Whether you are a die-hard Free Software advocate or are just looking to write a commercial application or game, there are no restrictions on what can be built on the Linux app ecosystem. How can we navigate the complex world of licenses to create great products?

What kind of open source business models exist through licensing? What are the latest issues related to licensing that our community should be aware of?

## Important Dates:

* Call for Papers opens: February 7th
* Call for Papers closes: March 22nd
* Speakers announced: April 1st

<button class="button" onclick="window.location.href='https://conf.linuxappsummit.org/event/3/abstracts/#submit-abstract'">Submit your talk</button>
